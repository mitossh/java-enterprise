package com.hillel.carStore.client.someapi;

import com.hillel.carStore.core.domain.model.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@FeignClient(name = "someapi", url = "${client.someapi.url}")
@Component
@RequestMapping("/users")
public interface SomeClient {

    @RequestMapping(method = RequestMethod.GET)
    List<User> getAllUsers();

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    User getUser(@PathVariable(name = "id") long id);

    
}
