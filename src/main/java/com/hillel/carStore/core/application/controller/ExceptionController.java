package com.hillel.carStore.core.application.controller;

import com.hillel.carStore.core.application.dto.ApiError;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;

import java.time.LocalDateTime;

@ControllerAdvice
public class ExceptionController {

    public ResponseEntity<ApiError> handle(MethodArgumentNotValidException exception) {
        return new ResponseEntity<ApiError>(new ApiError(LocalDateTime.now(), exception.getMessage()),
                HttpStatus.BAD_REQUEST);
    }
}
