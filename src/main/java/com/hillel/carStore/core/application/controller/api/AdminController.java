package com.hillel.carStore.core.application.controller.api;

import com.hillel.carStore.core.application.facade.UserFacade;
import io.swagger.annotations.Api;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/admin/users")
@Api(value = "Users operations for Admin", tags = "Operation for delete User", protocols = "/admin/users")
public class AdminController {

    private final UserFacade userFacade;

    public AdminController(UserFacade userFacade) {
        this.userFacade = userFacade;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void deleteUser(@PathVariable Long id){
        userFacade.deleteUser(id);
    }
}
