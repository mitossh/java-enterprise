package com.hillel.carStore.core.application.controller.api;

import com.hillel.carStore.core.application.dto.UserDto;
import com.hillel.carStore.core.application.facade.SomeClientFacade;
import io.swagger.annotations.Api;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(value = "Users operations in the Some Client Controller", tags = "CRUD Operations for working with Users by Feign", protocols = "/feign/users")
@RestController
@RequestMapping("/api/feign/users")
public class SomeClientController {

    private final SomeClientFacade someClientFacade;

    public SomeClientController(SomeClientFacade someClientFacade) {
        this.someClientFacade = someClientFacade;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<UserDto> getAllUsers() {
        return someClientFacade.getAllUsers();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public UserDto getUser(@PathVariable Long id) {
        return someClientFacade.getUser(id);
    }
}
