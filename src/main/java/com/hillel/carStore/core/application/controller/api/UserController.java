package com.hillel.carStore.core.application.controller.api;

import com.hillel.carStore.core.application.dto.AddUserRequestDto;
import com.hillel.carStore.core.application.dto.UserDto;
import com.hillel.carStore.core.application.facade.UserFacade;
import io.swagger.annotations.Api;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(value = "Users operations in the car store", tags = "CRUD Operations for working with Users", protocols = "/carstore")
@RestController
@RequestMapping("/api/users")
public class UserController {

    private final UserFacade userFacade;

    public UserController(UserFacade userFacade) {
        this.userFacade = userFacade;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<UserDto> getAllUsers() {
        return userFacade.getAllUsers();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public UserDto getUser(@PathVariable Long id) {
        return userFacade.getUser(id);
    }

    @PatchMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public Long updateUser(@PathVariable() Long id, @RequestBody AddUserRequestDto addUserRequestDto){
        return userFacade.updateUser(id, addUserRequestDto);
    }

}
