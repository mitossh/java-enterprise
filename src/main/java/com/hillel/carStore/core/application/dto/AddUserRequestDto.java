package com.hillel.carStore.core.application.dto;


import com.hillel.carStore.core.application.validator.UserAgeConstraint;
import com.hillel.carStore.core.database.entity.Gender;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.*;

@ApiModel(value = "Add User Request", description = "Request Model for creating user")
@Getter
@Setter
public class AddUserRequestDto {

    @ApiModelProperty(value = "User gender", example = "MALE", required = true)
    @NotNull
    private Gender gender;

    @ApiModelProperty(value = "User firs name", example = "Ivan", required = true)
    @NotBlank
    private String firstName;

    @ApiModelProperty(value = "User last name", example = "Ivanov", required = true)
    @NotBlank
    private String lastName;

    @ApiModelProperty(value = "User email", example = "Ivanov@mail.com", required = true)
    @NotNull
    @Email
    private String email;

    @ApiModelProperty(value = "User password", example = "Q123we4", required = true)
    @NotNull
    @Size(min = 6)
    private String password;

    @ApiModelProperty(value = "User age", example = "21", required = true)
    @UserAgeConstraint
    private int age;
}
