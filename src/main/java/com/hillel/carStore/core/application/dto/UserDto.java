package com.hillel.carStore.core.application.dto;

import com.hillel.carStore.core.application.validator.UserAgeConstraint;
import com.hillel.carStore.core.database.entity.Gender;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


public class UserDto {

    @NotNull
    private Long id;

    @ApiModelProperty(value = "User gender", example = "MALE", required = true)
    @NotNull
    private Gender gender;

    @ApiModelProperty(value = "User firs name", example = "Ivan", required = true)
    @NotBlank
    private String firstName;

    @ApiModelProperty(value = "User last name", example = "Ivanov", required = true)
    @NotBlank
    private String lastName;

    @ApiModelProperty(value = "User email", example = "Ivanov@mail.com", required = true)
    @NotNull
    @Email
    private String email;

    @ApiModelProperty(value = "User age", example = "21", required = true)
    @UserAgeConstraint
    private Integer age;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }


}
