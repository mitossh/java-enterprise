package com.hillel.carStore.core.application.facade;

import com.hillel.carStore.core.application.dto.UserDto;
import com.hillel.carStore.core.domain.service.SomeClientService;
import com.hillel.carStore.core.mapper.SomeClientMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class SomeClientFacade {

    private final SomeClientService someClientService;
    private final SomeClientMapper someClientMapper;

    public SomeClientFacade(SomeClientService someClientService,
                            SomeClientMapper someClientMapper) {
        this.someClientService = someClientService;
        this.someClientMapper = someClientMapper;
    }


    public List<UserDto> getAllUsers() {
        return someClientService.getAllSomeUsers().stream()
                .map(someClientMapper::toDto)
                .collect(Collectors.toList());
    }

    public UserDto getUser(Long id) {
        return someClientMapper.toDto(someClientService.getUser(id));
    }
}
