package com.hillel.carStore.core.application.facade;

import com.hillel.carStore.core.application.dto.AddUserRequestDto;
import com.hillel.carStore.core.application.dto.UserDto;
import com.hillel.carStore.core.domain.service.UserService;
import com.hillel.carStore.core.mapper.UserMapper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class  UserFacade {
    private final UserService userService;
    private final UserMapper userMapper;

    public UserFacade(UserService userService, UserMapper userMapper) {
        this.userService = userService;
        this.userMapper = userMapper;
    }

    public List<UserDto> getAllUsers() {
        return userService.getAllUsers().stream()
                .map(userMapper::toDto)
                .collect(Collectors.toList());
    }

    public UserDto getUser(Long id) {
        return userMapper.toDto(userService.getUser(id));
    }

    public Long addUser(AddUserRequestDto addUserRequestDto) {
        return userService.addUser(userMapper.fromDtoToModel(addUserRequestDto));
    }

    public Long updateUser(Long id, AddUserRequestDto addUserRequestDto) {
        return userService.updateUser(id, userMapper.fromDtoToModel(addUserRequestDto));
    }

    public void deleteUser(Long id) {
        userService.deleteUser(id);
    }
}
