package com.hillel.carStore.core.database.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "car")
public class Car {
    @Id
    @GeneratedValue(generator = "user_generator")
    @SequenceGenerator(name = "user_generator", sequenceName = "user_sequence", initialValue = 1)
    private Long id;

    private String model;
    private String colour;

    @ManyToMany(mappedBy = "cars")
    private List<Cart> carts;

}
