package com.hillel.carStore.core.database.entity;

public enum Gender {
    MALE, FEMALE;
}
