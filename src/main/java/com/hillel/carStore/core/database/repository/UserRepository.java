package com.hillel.carStore.core.database.repository;

import com.hillel.carStore.core.database.entity.UserEntity;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends PagingAndSortingRepository<UserEntity, Long> {

    Optional<UserEntity> findByEmail(String email);

    Optional<UserEntity> findByFirstNameAndLastName(String firstName, String lastName);

    boolean existsByEmail(String email);

    List<UserEntity> findAllByOrderByFirstNameAsc();

    List<UserEntity> findAllByOrderByAgeDesc();

    UserEntity getById(long id);

}