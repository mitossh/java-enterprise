package com.hillel.carStore.core.domain.model;

import com.hillel.carStore.core.database.entity.Gender;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class User {
    private Long id;
    private Gender gender;
    private String firstName;
    private String lastName;
    private String email;
    private Integer age;
}
