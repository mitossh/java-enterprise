package com.hillel.carStore.core.domain.service;

import com.hillel.carStore.client.someapi.SomeClient;
import com.hillel.carStore.core.domain.model.User;
import com.hillel.carStore.core.mapper.SomeClientMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SomeClientService {

    private final SomeClient someClient;
    private final SomeClientMapper someClientMapper;


    public SomeClientService(SomeClient someClient,
                             SomeClientMapper someClientMapper) {
        this.someClient = someClient;
        this.someClientMapper = someClientMapper;
    }

    public List<User> getAllSomeUsers() {
        return someClient.getAllUsers();
    }


    public User getUser(Long id) {
        return someClientMapper.toModel(someClientMapper.toDto(someClient.getUser(id)));
    }
}
