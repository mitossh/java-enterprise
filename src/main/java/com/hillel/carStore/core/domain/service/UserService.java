package com.hillel.carStore.core.domain.service;

import com.hillel.carStore.core.database.entity.Cart;
import com.hillel.carStore.core.database.entity.UserEntity;
import com.hillel.carStore.core.database.repository.UserRepository;
import com.hillel.carStore.core.domain.model.AddUserRequest;
import com.hillel.carStore.core.domain.model.User;
import com.hillel.carStore.core.domain.utils.ApiException;
import com.hillel.carStore.core.mapper.UserMapper;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;

    public UserService(UserRepository userRepository, UserMapper userMapper) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
    }

    public List<User> getAllUsers() {
        return StreamSupport.stream(userRepository.findAll().spliterator(), false)
                .map(userMapper::toModel)
                .collect(Collectors.toList());
    }

    public User getUser(long id) {
        return userMapper.toModel(userRepository.getById(id));
    }


    public Long addUser(AddUserRequest addUserRequest) {
        UserEntity userEntity = userMapper.toEntityRequest(addUserRequest);
        String email = userEntity.getEmail();
        if(userRepository.existsByEmail(email)) throw new ApiException("ОШИБКА!");

        Cart cartEntity = new Cart();
        userEntity.setCart(cartEntity);
        userRepository.save(userEntity);

        return userEntity.getId();

    }

    public Long updateUser(Long id, AddUserRequest addUserRequest) {
        if (!userRepository.existsById(id)) throw new ApiException("ОШИБКА!");
        UserEntity entity = userMapper.toEntityRequest(addUserRequest);
        entity.setId(id);
        userRepository.save(entity);
        return entity.getId();
    }

    public void deleteUser(Long id) {
        if(!userRepository.existsById(id)) throw  new ApiException("ОШИБКА!");
        userRepository.deleteById(id);

    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        UserEntity userEntity = userRepository.findByEmail(email)
                .orElseThrow(() -> new UsernameNotFoundException(""));
        return userMapper.toModelRequest(userEntity);
    }
}
