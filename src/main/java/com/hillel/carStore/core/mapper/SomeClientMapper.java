package com.hillel.carStore.core.mapper;

import com.hillel.carStore.core.application.dto.UserDto;
import com.hillel.carStore.core.domain.model.User;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class SomeClientMapper {

    private final ModelMapper modelMapper;

    public SomeClientMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }


    public UserDto toDto(User user) {
        return Objects.isNull(user) ? null : modelMapper.map(user, UserDto.class);
    }

    public User toModel(UserDto userDto) {
        return Objects.isNull(userDto) ? null : modelMapper.map(userDto, User.class);
    }

}
