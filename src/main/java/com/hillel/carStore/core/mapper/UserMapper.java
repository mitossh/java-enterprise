package com.hillel.carStore.core.mapper;

import com.hillel.carStore.core.application.dto.AddUserRequestDto;
import com.hillel.carStore.core.application.dto.UserDto;
import com.hillel.carStore.core.database.entity.UserEntity;
import com.hillel.carStore.core.domain.model.AddUserRequest;
import com.hillel.carStore.core.domain.model.User;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class UserMapper {

    private final PasswordEncoder passwordEncoder;
    private final ModelMapper mapper;


    public UserMapper(PasswordEncoder passwordEncoder, ModelMapper mapper) {
        this.passwordEncoder = passwordEncoder;
        this.mapper = mapper;
    }

    public UserDto toDto(User user) {
        return Objects.isNull(user) ? null : mapper.map(user, UserDto.class);
    }

    public User toModel(UserEntity userEntity) {
        return Objects.isNull(userEntity) ? null : mapper.map(userEntity, User.class);
    }

    public AddUserRequest toModelRequest(UserEntity userEntity) {
        return Objects.isNull(userEntity) ? null : mapper.map(userEntity, AddUserRequest.class);
    }

    public User toModel(UserDto userDto) {
        return Objects.isNull(userDto) ? null : mapper.map(userDto, User.class);
    }

    public UserEntity toEntityRequest(AddUserRequest addUserRequest) {
        UserEntity userEntity = mapper.map(addUserRequest, UserEntity.class);
        userEntity.setPassword(passwordEncoder.encode(addUserRequest.getPassword()));
        return userEntity;
    }

    public UserEntity toEntity(AddUserRequest addUserRequest) {
        return Objects.isNull(addUserRequest) ? null : mapper.map(addUserRequest, UserEntity.class);
    }

    public AddUserRequest fromDtoToModel(AddUserRequestDto userDto) {
        return Objects.isNull(userDto) ? null : mapper.map(userDto, AddUserRequest.class);
    }

    public UserDto toIdUserDto(User user) {
        UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        return userDto;
    }

}
