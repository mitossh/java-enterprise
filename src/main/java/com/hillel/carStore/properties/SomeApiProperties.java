package com.hillel.carStore.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties("client.someapi")
public class SomeApiProperties {
    private String url;
}
