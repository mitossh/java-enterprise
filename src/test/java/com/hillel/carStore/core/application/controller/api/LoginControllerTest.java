package com.hillel.carStore.core.application.controller.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hillel.carStore.CarStoreApplication;
import com.hillel.carStore.core.application.dto.AddUserRequestDto;
import com.hillel.carStore.core.application.dto.LoginRequestDto;
import com.hillel.carStore.core.application.dto.UserDto;
import com.hillel.carStore.core.database.entity.Gender;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static com.hillel.carStore.core.application.controller.util.TestUtil.convertObjectToJsonBytes;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = {CarStoreApplication.class})
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class LoginControllerTest {

    private static Long registeredUserId;

    @Autowired
    private LoginController loginController;

    @Autowired
    AdminController adminController;

    @Autowired
    private ObjectMapper objectMapper;


    private MockMvc mockMvc;

    @BeforeEach
    public void init() {
        mockMvc = MockMvcBuilders.standaloneSetup(loginController).build();
    }

    @Test
    @Order(1)
    void login() throws Exception {
        LoginRequestDto loginRequestDto = new LoginRequestDto();
        loginRequestDto.setUsername("test@mail.com");
        loginRequestDto.setPassword("123456");

        byte[] bytes = convertObjectToJsonBytes(loginRequestDto);
        MockHttpServletRequestBuilder builder = post("/login")
                .contentType(APPLICATION_JSON)
                .content(bytes);

        assertThrows(Exception.class, () -> mockMvc.perform(builder));
    }

    @Test
    @Order(2)
    void registration() throws Exception {
        AddUserRequestDto userRequestDto = new AddUserRequestDto();
        userRequestDto.setPassword("123456");
        userRequestDto.setGender(Gender.MALE);
        userRequestDto.setLastName("LastName");
        userRequestDto.setFirstName("FirstName");
        userRequestDto.setEmail("test2@mail.com");
        userRequestDto.setAge(30);

        byte[] bytes = convertObjectToJsonBytes(userRequestDto);
        MockHttpServletRequestBuilder builder = post("/registration")
                .contentType(APPLICATION_JSON)
                .content(bytes);

        MvcResult mvcResult = mockMvc.perform(builder)
                .andExpect(status().isCreated())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andReturn();

        registeredUserId = objectMapper
                .readValue(mvcResult.getResponse().getContentAsString(), UserDto.class).getId();
    }


    @Test
    @Order(3)
    void clearTestedEntity() throws Exception {
        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(adminController).build();
        MockHttpServletRequestBuilder builder = delete("/api/admin/users/".concat(String.valueOf(registeredUserId)));
        mockMvc.perform(builder).andExpect(status().isAccepted());
    }
}