package com.hillel.carStore.core.application.controller.api;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hillel.carStore.CarStoreApplication;
import com.hillel.carStore.core.application.dto.AddUserRequestDto;
import com.hillel.carStore.core.application.dto.UserDto;
import com.hillel.carStore.core.database.entity.Gender;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static com.hillel.carStore.core.application.controller.util.TestUtil.convertObjectToJsonBytes;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest(classes = {CarStoreApplication.class})
class UserControllerTest {

    @Autowired
    private UserController userController;


    @Autowired
    private ObjectMapper objectMapper;

    private MockMvc mockMvc;

    @BeforeEach
    public void init() {
        mockMvc = MockMvcBuilders.standaloneSetup(userController).build();
    }

    @Test
    void getAllUsers() throws Exception{
        MockHttpServletRequestBuilder builder = get("/api/users")
                .contentType(APPLICATION_JSON);
        MvcResult mvcResult = mockMvc.perform(builder)
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andReturn();


        String response = mvcResult.getResponse().getContentAsString();
        List<UserDto> dtoList = objectMapper.readValue(response, new TypeReference<List<UserDto>>() {
        });

        assertNotNull(dtoList);
        assertNotEquals(0, dtoList);


    }

    @Test
    void getUser() throws Exception {
        MockHttpServletRequestBuilder builder = get("/api/users/1452")
                .contentType(APPLICATION_JSON);

        MvcResult mvcResult = mockMvc.perform(builder)
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andReturn();

        String response = mvcResult.getResponse().getContentAsString();
        UserDto userById = objectMapper.readValue(response, UserDto.class);
        assertNotNull(userById);
        assertEquals(1452, userById.getId());

    }

    @Test
    void updateUser() throws Exception {

        AddUserRequestDto user = new AddUserRequestDto();
        user.setAge(25);
        user.setEmail("test@mail.ua");
        user.setGender(Gender.FEMALE);
        user.setFirstName("newUserTestFirst");
        user.setLastName("TestLast");
        user.setPassword("123456");

        byte[] bytes = convertObjectToJsonBytes(user);
        MockHttpServletRequestBuilder builder = patch("/api/users/1452")
                .contentType(APPLICATION_JSON)
                .content(bytes);

       mockMvc.perform(builder)
                .andExpect(status().isOk());

    }
}