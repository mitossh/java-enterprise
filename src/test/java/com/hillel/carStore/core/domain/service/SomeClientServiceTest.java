package com.hillel.carStore.core.domain.service;

import com.hillel.carStore.client.someapi.SomeClient;
import com.hillel.carStore.core.database.entity.Gender;
import com.hillel.carStore.core.domain.model.User;
import com.hillel.carStore.core.mapper.SomeClientMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class SomeClientServiceTest {

    @InjectMocks
    private SomeClientService someClientService;

    @Mock
    private SomeClient someClient;
    @Mock
    private SomeClientMapper someClientMapper;

    private User user;
    @BeforeEach
    public void setUp() {
        user = new User();
        user.setId(1l);
        user.setFirstName("user1");
        user.setLastName("user1User");
        user.setAge(30);
        user.setEmail("user@mail.ua");
        user.setGender(Gender.MALE);

    }

    @Test
    void getAllSomeUsers() {

        when(someClient.getAllUsers()).thenReturn(Collections.singletonList(new User()));

        List<User> expected = someClient.getAllUsers();
        assertNotNull(expected);
        assertEquals(1, expected.size());
    }

    @Test
    void getUser() {
        when(someClient.getUser(anyLong())).thenReturn(new User());
        when(someClientMapper.toModel(someClientMapper.toDto(any(User.class)))).thenReturn(user);

        User userById = someClientService.getUser(anyLong());
        assertNotNull(userById);
        assertEquals("user1", userById.getFirstName());
    }
}