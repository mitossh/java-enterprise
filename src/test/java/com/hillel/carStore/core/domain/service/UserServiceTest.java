package com.hillel.carStore.core.domain.service;

import com.hillel.carStore.core.database.entity.Gender;
import com.hillel.carStore.core.database.entity.UserEntity;
import com.hillel.carStore.core.database.repository.UserRepository;
import com.hillel.carStore.core.domain.model.AddUserRequest;
import com.hillel.carStore.core.domain.model.User;
import com.hillel.carStore.core.mapper.UserMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @InjectMocks
    private UserService userService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private UserMapper userMapper;

    private User user;
    @BeforeEach
    public void setUp() {
        user = new User();
        user.setId(1l);
        user.setFirstName("user1");
        user.setLastName("user1User");
        user.setAge(30);
        user.setEmail("user@mail.ua");
        user.setGender(Gender.MALE);

    }

    @Test
    void getAllUsers() {
        when(userRepository.findAll()).thenReturn(Collections.singletonList(new UserEntity()));
        when(userMapper.toModel(any(UserEntity.class))).thenReturn(user);

        List<User> expected = userService.getAllUsers();
        assertNotNull(expected);
        assertEquals(1, expected.size());
        assertEquals("user@mail.ua", expected.get(0).getEmail());

    }

    @Test
    void getUser() {
        when(userRepository.getById(anyLong())).thenReturn(new UserEntity());
        when(userMapper.toModel(any(UserEntity.class))).thenReturn(user);

        User userById = userService.getUser(anyLong());
        assertNotNull(userById);
        assertEquals("user1", userById.getFirstName());
    }

    @Test
    void addUser() {
        AddUserRequest addUserRequest = new AddUserRequest();

        UserEntity userEntity = new UserEntity();
        userEntity.setId(1L);
        userEntity.setAge(25);
        userEntity.setEmail("test@mail.ua");
        userEntity.setGender(Gender.FEMALE);
        userEntity.setFirstName("TestFirst");
        userEntity.setLastName("TestLast");

        when(userMapper.toEntityRequest(any(AddUserRequest.class))).thenReturn(userEntity);
        when(userRepository.existsByEmail(anyString())).thenReturn(false);
        when(userRepository.save(any(UserEntity.class))).thenReturn(userEntity);

        Long addUserId = userService.addUser(addUserRequest);

        assertEquals(1L, addUserId);

        verify(userRepository, times(1)).save(any(UserEntity.class));
        verifyNoMoreInteractions(userRepository);

    }

    @Test
    void updateUser() {
        AddUserRequest user = new AddUserRequest();

        UserEntity userEntity = new UserEntity();
        userEntity.setId(1L);
        userEntity.setAge(25);
        userEntity.setEmail("test@mail.ua");
        userEntity.setGender(Gender.FEMALE);
        userEntity.setFirstName("newUserTestFirst");
        userEntity.setLastName("TestLast");

        when(userRepository.existsById(anyLong())).thenReturn(true);
        when(userMapper.toEntityRequest(any(AddUserRequest.class))).thenReturn(userEntity);
        userService.updateUser(anyLong(), user);

        assertEquals("newUserTestFirst", userEntity.getFirstName());

        verify(userRepository, times(1)).save(any(UserEntity.class));
        verifyNoMoreInteractions(userRepository);


    }

    @Test
    void deleteUser() {
        when(userRepository.existsById(anyLong())).thenReturn(true);
        userService.deleteUser(anyLong());

        verify(userRepository, times(1)).deleteById(anyLong());
        verifyNoMoreInteractions(userRepository);

    }

    @Test
    void loadUserByUsername() {
        UserEntity userEntity = new UserEntity();

        AddUserRequest user = new AddUserRequest();
        user.setAge(24);
        user.setEmail("test@mail.ru");
        user.setGender(Gender.FEMALE);
        user.setFirstName("Test");
        user.setLastName("Test");
        user.setPassword("Test");

        when(userRepository.findByEmail(anyString())).thenReturn(Optional.of(userEntity));
        when(userMapper.toModelRequest(any())).thenReturn(user);

        UserDetails userDetails = userService.loadUserByUsername("test@mail.ru");

        assertEquals("test@mail.ru", userDetails.getUsername());

        verify(userRepository, times(1)).findByEmail(anyString());
        verifyNoMoreInteractions(userRepository);

        verify(userMapper, times(1)).toModelRequest(any());
        verifyNoMoreInteractions(userMapper);
    }
}